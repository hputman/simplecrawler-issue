# README #

Demonstrates an issue with [node-simplecrawler](https://github.com/cgiffard/node-simplecrawler) on Windows when using filesystem to cache fetched data.

### What happened (Observed behaviour)
Crawler fails when caching page link includes Query string.
```
C:\path-redacted\node_modules\simplecrawler\lib\cache-backend-fs.js:161
                throw error;
                ^

Error: ENOENT: no such file or directory, open 'C:\path-redacted\cache\https\simplecrawler-issue.aerobatic.io\page2?06c2da62493e8546b05a26cbc40588562d6b5abb.html'
    at Error (native)
```
### What should happen (Expected behaviour)
It should crawl fine.

### Steps to reproduce the problem (include code examples)
```
var Crawler = require("simplecrawler");

var crawler = new Crawler("https://simplecrawler-issue.aerobatic.io");
crawler.cache = new Crawler.cache('cache');

crawler.start();
```

Site pages are at: [here](https://bitbucket.org/hputman/simplecrawler-issue).

The issue goes away if I remove the call to specify the local filesystem cache directory. I can also fix it by adding this to the writeFileData function in `cache-backend-fs.js` (line 160).
```
        if (os.platform() === "win32"  && currentPath.indexOf("?") >= 0) {
          currentPath = currentPath.replace(/\?/g, "_");
        }
``` 
But I am not sure what the implications of mangling the cache data filenames might be.